<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="contextRoot" value="${pageContext.request.contextPath}"></c:set>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Expense</title>
<style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f2f2f2;
        margin: 20px;
        padding: 0;
    }

    .container {
        max-width: 600px;
        margin: auto;
        background: #fff;
        padding: 20px;
        border-radius: 8px;
        box-shadow: 0 0 10px rgba(0,0,0,0.1);
    }

    h1 {
        text-align: center;
        margin-bottom: 20px;
    }

    form {
        display: grid;
        gap: 10px;
    }

    form input[type="text"],
    form input[type="number"],
    form textarea {
        width: calc(100% - 20px);
        padding: 10px;
        font-size: 16px;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }

    form button {
        width: 100%;
        padding: 10px;
        background-color: #4CAF50;
        color: white;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        font-size: 16px;
    }

    form button:hover {
        background-color: #45a049;
    }

    a {
        display: block;
        text-align: center;
        margin-top: 10px;
        text-decoration: none;
        color: #666;
    }

    a:hover {
        color: #333;
    }
</style>
</head>
<body>
    <div class="container">
        <h1>Add Expense</h1>

        <form:form action="${contextRoot}/expense" method="post"
            modelAttribute="expense">
            
            <form:hidden path="id" />
            
            <input type="text" path="description" placeholder="Enter expense name"/>
            
            <input type="number" path="amount" placeholder="Enter expense amount"/>
            
            <input type="number" path="quantity" placeholder="Enter quantity" />
            
            <textarea path="note" placeholder="Enter note"></textarea>
            
            <button type="submit">Add Expense</button>
        </form:form>
        
        <a href="${contextRoot}/expense/${expense.id}/delete">Delete</a>
    </div>
</body>
</html>
