<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="contextRoot" value="${pageContext.request.contextPath}"></c:set>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Expense Tracker</title>
<style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f2f2f2;
        margin: 20px;
        text-align: center;
    }

    h1 {
        color: #333;
        margin-bottom: 20px;
    }

    .center-content {
        display: flex;
        justify-content: center;
        margin-bottom: 20px;
    }

    .center-content a {
        display: inline-block;
        padding: 10px 20px;
        background-color: #4CAF50;
        color: white;
        text-decoration: none;
        border-radius: 4px;
    }

    .center-content a:hover {
        background-color: #45a049;
    }

    table {
        width: 80%;
        margin: 0 auto;
        border-collapse: collapse;
        background-color: #fff;
        box-shadow: 0 0 10px rgba(0,0,0,0.1);
    }

    th, td {
        border: 1px solid #ddd;
        padding: 12px;
        text-align: left;
    }

    th {
        background-color: #f2f2f2;
    }

    td {
        font-size: 14px;
    }

    .actions {
        white-space: nowrap;
    }

    .actions a {
        margin-right: 8px;
        color: #333;
        text-decoration: none;
        transition: color 0.3s ease;
    }

    .actions a:hover {
        color: #45a049;
        font-weight: bold;
    }
</style>
</head>
<body>

    <h1>Expense Tracker</h1>
    <p>${message}</p>
    
    <div class="center-content">
        <a href="${contextRoot}/expense">Add Expense</a>
    </div>

    <table>
        <thead>
            <tr>
                <th>Description</th>
                <th>Amount</th>
                <th>Quantity</th>
                <th>Note</th>
                <th class="actions">Actions</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="expense" items="${expenses}">
                <tr>
                    <td>${expense.description}</td>
                    <td>&#8377;${expense.amount}</td>
                    <td>${expense.quantity}</td>
                    <td>${expense.note}</td>
                    <td class="actions">
                        <a href="${contextRoot}/expense/${expense.id}" class="edit-link">Edit</a>
                        <a href="${contextRoot}/expense/${expense.id}/delete" class="delete-link">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

</body>
</html>
