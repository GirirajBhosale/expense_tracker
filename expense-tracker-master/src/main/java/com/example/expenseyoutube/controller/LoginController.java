package com.example.expenseyoutube.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.expenseyoutube.model.Login;
import com.example.expenseyoutube.service.LoginService;

@Controller
public class LoginController {
    
    @Autowired
    LoginService loginService;

    @RequestMapping("/login")
    public ModelAndView login() {
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("login", new Login());
        return mav;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute("login") Login login) {
        if (loginService.validate(login.getUsername(), login.getPassword())) {
            return "redirect:/"; // Redirect to home page if login is successful
        }
        return "login"; // Return to login page if login fails
    }
}
