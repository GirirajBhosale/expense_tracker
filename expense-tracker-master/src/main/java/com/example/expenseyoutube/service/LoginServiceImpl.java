package com.example.expenseyoutube.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.expenseyoutube.repository.LoginRepository;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    LoginRepository loginRepository;

    @Override
    public boolean validate(String username, String password) {
        return loginRepository.existsByUsernameAndPassword(username, password);
    }
}
