package com.example.expenseyoutube.service;

public interface LoginService {
    boolean validate(String username, String password);
}
