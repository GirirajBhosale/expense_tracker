package com.example.expenseyoutube.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.expenseyoutube.model.Login;

public interface LoginRepository extends JpaRepository<Login, Long> {
    boolean existsByUsernameAndPassword(String username, String password);
}

